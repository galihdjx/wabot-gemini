import { ai } from "../app/ai.js";

const aiChat = async (prompt) => {
    try {
        const result = await ai.generateContent(prompt);
        const response = await result.response;
        const answer = response.text();
        return answer;
      } catch (error) {
        if (error.code === 429) {
          const retryAfter = error.details.retryAfter;
          console.log(`Melebihi batas./nTunggu hingga ${retryAfter / 1000} detik dan coba lagi`);
          await setTimeout(() => {}, retryAfter);
          return await generateContent(prompt);
        } else {
          throw error;
        }
      }
}

export {
    aiChat
}
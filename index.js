import { aiChat } from "./services/genAi.js";
import waweb from 'whatsapp-web.js';
const {Client, LocalAuth} = waweb;
import qrcode from "qrcode-terminal";

const client = new Client({
    authStrategy: new LocalAuth({
        dataPath: './authwa'
    })
});

client.on('qr', (qr) => {
    qrcode.generate(qr, { small: true }); 
});

client.on('ready', () => {
    console.log('Client is ready!');
});

client.on('message', async (message) => {
    if(message.body.startsWith('.ai')){
        const prompt = message.body.replace('.ai', '');
        const answer = await aiChat(prompt);
        message.reply(answer);
    }
})

client.initialize();